package com.twuc;

import java.util.ArrayList;
import java.util.List;

public class TotalScore {
    List<Frame> list = new ArrayList<>();
    private int totalScore;

//    public int getTotalScore() {
//       for (int i=0;i<10;i++){
//           totalScore += list.get(i).getFrameScore();
//       }
//       return totalScore;
//    }
    public int getTotalScore() {
        for (int i = 0; i < 10; i++) {
            if (list.get(i).getThrow1()==10){
                totalScore+=10;
                totalScore+=list.get(i+1).getFrameScore();
            }else if (list.get(i).getFrameScore()==10 && i <9 &list.get(i).getThrow1()!=10){
                totalScore+=10;
                totalScore+=list.get(i+1).getThrow1();
            }else{
                totalScore += list.get(i).getFrameScore();
            }
        }
        if (list.get(9).getFrameScore()==10){
            Bowling bowling =new Bowling();
            bowling.setThrows(1);
            totalScore+=bowling.getTotleScore();
        }

        return totalScore;
    }

    public List<Frame> getList() {
        return list;
    }
}
