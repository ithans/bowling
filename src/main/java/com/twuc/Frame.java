package com.twuc;

public class Frame {
    public Frame(int throw1) {
        this.throw1=throw1;
    }

    public int getThrow1() {
        return throw1;
    }

    private int throw1;
    private int throw2;
    private int frameScore;

    public Frame(int throw1, int throw2) {
        this.throw1=throw1;
        this.throw2=throw2;
    }
    public int getFrameScore() {
        return throw1+throw2;
    }
}
