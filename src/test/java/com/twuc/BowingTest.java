package com.twuc;

import org.junit.Test;


import java.util.List;

import static org.junit.Assert.assertEquals;

public class BowingTest {
    @Test
    public void should_return_0_when_throw_num_0() {
        Bowling bowling = new Bowling();
        assertEquals(0, bowling.getTotleScore());
    }

    @Test
    public void should_return_value_of_one_throw() {
        Bowling bowling = new Bowling();
        bowling.setThrows(5);
        assertEquals(5, bowling.getTotleScore());
    }

    @Test
    public void should_return_total_score_when_two_throws() {
        Frame frame = new Frame(3, 6);
        assertEquals(9, frame.getFrameScore());
    }

    @Test
    public void should_return_total_score_when_with_space_and_not_strike() {
        Frame frame1 = new Frame(10);
        Frame frame2 = new Frame(1, 3);
        Frame frame3 = new Frame(1, 3);
        Frame frame4 = new Frame(1, 3);
        Frame frame5 = new Frame(1, 3);
        Frame frame6 = new Frame(10);
        Frame frame7 = new Frame(1, 3);
        Frame frame8 = new Frame(1, 3);
        Frame frame9 = new Frame(1, 3);
        Frame frame10 = new Frame(7, 3);//28+30+8+1

        TotalScore total = new TotalScore();
        List<Frame> list = total.getList();
        list.add(frame1);
        list.add(frame2);
        list.add(frame3);
        list.add(frame4);
        list.add(frame5);
        list.add(frame6);
        list.add(frame7);
        list.add(frame8);
        list.add(frame9);
        list.add(frame10);
        assertEquals(67, total.getTotalScore());
    }

}
